# Use 'time' module
# Write clock.bin
# Readable from other app

import time
import os

clock = open("clock.txt","w")

while True:
    time.sleep(0.8)
    # Europe/Rome
    os.environ['TZ'] = 'Europe/Rome'
    time.tzset()
    clock.write("Europe/Rome ")
    year = time.strftime('%Y')
    clock.write(year+" ")
    month = time.strftime('%m')
    clock.write(month+" ")
    day = time.strftime('%d')
    clock.write(day+" ")
    hour = time.strftime('%H')
    clock.write(hour+" ")
    minute = time.strftime('%M')
    clock.write(minute+" ")
    # Europe/London
    os.environ['TZ'] = 'Europe/London'
    time.tzset()
    clock.write("Europe/London ")
    year = time.strftime('%Y')
    clock.write(year+" ")
    month = time.strftime('%m')
    clock.write(month+" ")
    day = time.strftime('%d')
    clock.write(day+" ")
    hour = time.strftime('%H')
    clock.write(hour+" ")
    minute = time.strftime('%M')
    clock.write(minute+" ")
    break
